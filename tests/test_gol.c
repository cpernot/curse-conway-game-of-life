#include <stdlib.h>
#include <stdio.h>
#include <check.h>
#include <limits.h>
#include "gol.h"
#include "render.h"
#include "rle.h"
#include "exception.h"


static void _print_game(game_t g, int x, int y, int width, int height) {
  for (int j = 0; j < height && y+j < game_get_height(g); j++) {
    printf("\n");
    for (int i = 0; i < width && x+i < game_get_width(g); i++)
      printf("%c",  game_get(g, x+i, y+j) ? 'O' : '.');
  }
}

static void _print_exception() {
  exception_code_t code;
  char *msg;
  exception_get(&code, &msg);
  printf("EXCEPTION %d : [%s]\n", code, msg);
}

static void compare_rle(char *a, char *b) {
  /* compare in and out, newlines taken apart */
  while (*a == '#') {
    a = strchr(a, '\n');
    if (a == NULL) break;
    a++;
  }
  while (*b == '#') {
    b = strchr(b, '\n');
    if (b == NULL) break;
    b++;
  }
  for (char *in = a, *out = b; ; in++, out++) {
    while (*in != '\0' && *in == '\n') in++;
    while (*out != '\0' && *out == '\n') out++;
    if (*in == '\0' || *out == '\0') {
      ck_abort_msg("Premature ending in compress");
    }
    ck_assert_msg( *in == *out, "Difference at index %d \"%.8s\" != \"%.8s\"",
		   (int)(in - a), in, out );
    if (*in == '!')
      break;
  }
}

START_TEST (test_game_init)
{
  game_t g;
  exception_code_t e_code;
  char *e_message;

  /* test allocation error */
  g = game_init(INT_MAX, INT_MAX, -1, NULL);
  ck_assert( g == NULL );
  exception_get(&e_code, &e_message);
  ck_assert(e_code == NO_MEMORY);
  exception_clear();

  /* test invalid height */
  g = game_init(128, -5, -1, NULL);
  ck_assert( g == NULL );
  exception_get(&e_code, &e_message);
  ck_assert(e_code == INVALID_PARAMETER);
  exception_clear();

}
END_TEST

START_TEST (test_game_empty)
{
  game_t g;
  render_context_t ctx;
  const int width = 64;
  const int height = 64;

  g = game_init(width, height, -1, NULL);
  ctx = render_init("dummy", g, false);

  game_empty(g);

  for (int i = 0; i < game_get_width(g); i++)
    for (int j = 0; j < game_get_height(g); j++) {
      ck_assert( game_get(g, i, j) == 0 );
  }
}
END_TEST

/* defined in test_rle.c */
extern char glider[];
extern bool glider_pattern[3][3];
extern char lidka[], lidka50[], lidka100[];
extern char glider_train[];
extern char twoguns[];
extern bool twoguns_pattern[27][39];

START_TEST (test_game_rle_put)
{
  game_t g;
  render_context_t ctx;
  const int width = 39;
  const int height = 27;
  int res;

  g = game_init(128, 128, -1, NULL);
  ctx = render_init("dummy", g, false);

  game_empty(g);

  res = rle_put(g, 50, 50, twoguns);
  ck_assert( res == 0);

  for (int j = 0; j < height; j++) {
    for (int i = 0; i < width; i++) {
      ck_assert( game_get(g, 50+i, 50+j) == twoguns_pattern[j][i] );
    }
  }
}
END_TEST

START_TEST (test_game_rle_compress)
{
  game_t g;
  render_context_t ctx;
  const int width = 3;
  const int height = 3;
  int res;
  char *buffer;

  g = game_init(200, 200, -1, NULL);
  ctx = render_init("dummy", g, false);

  game_empty(g);
  res = rle_put(g, 5, 15, glider);
  if (res != 0) {
    _print_exception();
    _print_game(g, 0, 0, 20, 20);
  }
  ck_assert( res == 0);

  buffer = rle_compress(g, 5, 15, 3, 3);
  if (buffer == NULL) {
        _print_exception();
	_print_game(g, 0, 0, 20, 20);
  }
  ck_assert( buffer != NULL );
  compare_rle(glider, buffer);

  game_empty(g);
  res = rle_put(g, 50, 15, glider_train);
  if (res != 0) {
    _print_exception();
  }
  ck_assert( res == 0);

  //_print_game(g, 50, 15, 68, 33);
  buffer = rle_compress(g, 50, 15, 68, 33);
  if (buffer == NULL)
        _print_exception();
  ck_assert( buffer != NULL );
  // printf("gilder_train:\n%s\n%s\n", glider_train, buffer);

  /*
  game_empty(g);
  res = rle_put(g, 50, 15, buffer);
  _print_game(g, 50, 15, 68, 33);
  */

  compare_rle(glider_train, buffer);


  free(buffer);
}
END_TEST


START_TEST (test_game_step)
{
  game_t g;
  render_context_t ctx;
  const int width = 128;
  const int height = 128;
  char *rle;
  int res;

  g = game_init(width, height, -1, NULL);
  ctx = render_init("dummy", g, false);

  game_empty(g);
  res = rle_put(g, 60, 50, lidka);
  ck_assert( res == 0);

  for (int i = 0; i < 50; i++)
    game_step(g, ctx);
  rle = rle_compress(g, 0, 0, 128, 128);
  //printf("\n%s\n%s\n", lidka50, rle);
  ck_assert( res == 0);

  //printf("\nlidka50\n%s", rle);
  //compare_rle(lidka50, rle);
  free(rle);

  for (int i = 0; i < 50; i++)
    game_step(g, ctx);
  rle = rle_compress(g, 0, 0, 128, 128);
  ck_assert( res == 0);
  //printf("\nlidka100\n%s", rle);
  compare_rle(lidka100, rle);
  free(rle);
}
END_TEST


static void add_test_case(Suite *s, char *name, void (*testfunc)(int)) {
  TCase *tc;

  tc = tcase_create(name);
  tcase_add_test(tc, testfunc);
  suite_add_tcase(s, tc);
}

Suite * gol_suite(void) {
  Suite *s;

  s = suite_create("Game of life");

  add_test_case(s, "init", test_game_init);
  add_test_case(s, "empty", test_game_empty);
  add_test_case(s, "rle put", test_game_rle_put);
  add_test_case(s, "rle compress", test_game_rle_compress);
  add_test_case(s, "step", test_game_step);

  return s;
}

int main(void)
{
  int number_failed;
  Suite *s;
  SRunner *sr;

  s = gol_suite();
  sr = srunner_create(s);

  srunner_run_all(sr, CK_ENV);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;

}
