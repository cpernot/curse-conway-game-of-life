#include <stdlib.h>
#include <stdbool.h>
#include <check.h>
#include "gol.h"
#include "render.h"

START_TEST (test_render_init)
{
  game_t game;
  render_context_t ctx;

  game = game_init(64, 64, 0, NULL);
  ck_assert_ptr_ne(game, NULL);

  ctx = render_init("dummy", game, false);
  ck_assert_ptr_ne(ctx, NULL);

  render_close(ctx);
}
END_TEST


Suite * render_suite(void) {
  Suite *s;
  TCase *tc_init;

  s = suite_create("Game of life renderer");

  /* "init" test case */
  tc_init = tcase_create("init");
  tcase_add_test(tc_init, test_render_init);
  suite_add_tcase(s, tc_init);

  return s;
}

int main(void)
{
  int number_failed;
  Suite *s;
  SRunner *sr;

  s = render_suite();
  sr = srunner_create(s);

  srunner_run_all(sr, CK_ENV);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;

}
