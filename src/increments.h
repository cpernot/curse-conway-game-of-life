#include <stdint.h>

extern void increments_init();

extern unsigned int v_increment32[0x100];
extern unsigned int h_increment32[0x100];
extern uint64_t v_increment64[0x10000];
extern uint64_t h_increment64[0x10000];
