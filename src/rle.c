#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <ctype.h>
#include "rle.h"
#include "exception.h"


char *_flush_cells(char *buffer, size_t *buffer_size,
		   int *write_index, size_t *line_start,
		   int *end_of_lines, int count, bool value) {

  char *_buffer;

  if (*buffer_size - *write_index < 70) {
    /* 70 is ridiculously too large. truth is that *size-index should never
       be less that 14, given how snprintf() and flush is made. */
    /* Increase buffer size by 10% */
    *buffer_size += *buffer_size / 10;
    _buffer = realloc(buffer, *buffer_size);
    if (_buffer == NULL) {
      free(buffer);
      exception_raise(NO_MEMORY, "buffer size too large : %d", *buffer_size);
      return NULL;
    }
    buffer = _buffer;
  }

  /* flush pending end of lines */
  if (*end_of_lines > 1) {
    *write_index += snprintf(buffer + *write_index, 12, "%d$", *end_of_lines);
    *end_of_lines = 0;
  } else if (*end_of_lines == 1) {
    buffer[(*write_index)++] = '$';
    *end_of_lines = 0;
  }

  if (count > 1)
    *write_index += snprintf(buffer + *write_index, 12, "%d%c", count, value ? 'o' : 'b');
  else
    buffer[(*write_index)++] = value ? 'o' : 'b';

  if (*write_index - *line_start > 65) {
    /* flush current line */
    buffer[(*write_index)++] = '\n';
    *line_start = *write_index;
  }
  return buffer;
}

/*
  Encode a subset of game as rle
*/
char *rle_compress(game_t g, int x, int y, int width, int height) {
  size_t buffer_size = 1024;
  char *buffer = malloc(buffer_size);
  size_t line_start = 0;
  int end_of_lines = 0;
  int write_index = 0;
  bool current;
  int count = 0;

  write_index += snprintf(buffer, buffer_size, "x = %d, y = %d\n", width, height);

  for (int j = 0; j < height; j++) {
    current = game_get(g, x, y+j);
    for (int i = 0; i < width; i++) {
      if (game_get(g, x+i, y+j) == current) {
	count++;
      } else {
	/* flush */
	buffer = _flush_cells(buffer, &buffer_size, &write_index,
			      &line_start, &end_of_lines, count, current);
	if (buffer == NULL) return NULL;
	current = !current;
	count = 1;
      }
    }

    /* end of line */
    if (current) {  /* dont write trailing dead cells */
      buffer = _flush_cells(buffer, &buffer_size, &write_index,
			    &line_start, &end_of_lines, count, current);
      if (buffer == NULL) return NULL;
    }
    count = 0;
    end_of_lines += 1;
  }

  buffer[write_index++] = '!';
  buffer[write_index++] = '\0';
  return buffer;
}


/*
  Draw rle-encoded pattern in game's grid at (x,y).
  Return 0 on success, -1 otherwise.
 */
int rle_put(game_t g, int x, int y, char *data) {
  long width = 0;
  long height = 0;
  long count = 1;
  bool done = false;
  bool error = false;
  int line = 0; // line currently parsed in data
  int i = 0, j = 0;
  enum {
	COMMENTS,
	HEADER,
	DATA
  } state = COMMENTS;

  /* parse header */
  while (*data && !done && !error) {
    switch (*data) {
    case 'x':
      if (state != COMMENTS) { error = true; break; }
      state = HEADER;
      data = strchr(data, '=');
      if (data == NULL) { error = true; break; }
      width = strtol(data+1, &data, 10);
      if (width == LONG_MIN || width == LONG_MAX) { error = true; break; }
      data = strchr(data, ',');
      if (data == NULL) { error = true; break; }
      while(isblank(*(data+1))) data++;
      break;
    case 'y':
      if (state != HEADER) { error = true; break; }
      data = strchr(data, '=');
      if (data == NULL) { error = true; break; }
      height = strtol(data+1, &data, 10);

      if (height == LONG_MIN || height == LONG_MAX) { error = true; break; }
      data = strchr(data, '\n');
      if (data == NULL) { error = true; break; }
      line++;
      state = DATA;
      break;
    case '#':
      /* eat-up comment until end of line */
      if (state != COMMENTS) { error = true; break; }
      data = strchr(data, '\n');
      line++;
      break;
    case '\n':
      if (state != DATA) { error = true; break; }
      line++;
      break;
    case '$':
      if (state != DATA) { error = true; break; }
      while (count>=1) {
	/* next line */
	while (i<width) {
	  game_set(g, x+i, y+j, 0);
	  i++;
	}
	i = 0;
	j++;
	count--;
      }
      count = 1;
      break;
    case '!':
      if (state != DATA) { error = true; break; }
      while (j<height) {
	while (i<width) {
	  game_set(g, x+i, y+j, 0);
	  i++;
	}
	i = 0;
	j++;
      }
      done = true;
      break;
    case 'o':
      if (state != DATA) { error = true; break; }
      while (count>=1) {
	game_set(g, x+i, y+j, 1);
	count --;
	i++;
      }
      count = 1;
      break;
    case 'b':
      if (state != DATA) { error = true; break; }
      while (count>=1) {
	game_set(g, x+i, y+j, 0);
	count --;
	i++;
      }
      count = 1;
      break;
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      if (state != DATA) { error = true; break; }
      count = strtol(data, &data, 10);
      if (count == LONG_MIN || count == LONG_MAX) { error = true; break; }
      while(isblank(*data)) data++;
      data--;
      break;
    default:
      error = true;
      break;
    }
    data++;
  }

  if (error || !done) {
    exception_raise(INVALID_FORMAT, "at line %d", line);
    return -1;
  }

  return 0;
}
