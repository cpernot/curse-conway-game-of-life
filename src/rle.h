#ifndef RLE_H
#define RLE_H

#include "gol.h"

extern char *rle_compress(game_t g, int x, int y, int width, int height);
extern int rle_put(game_t g, int x, int y, char *data);

#endif
