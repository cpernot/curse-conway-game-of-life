
#
# dependencies
#

find_package(SDL2 REQUIRED)
include_directories(${SDL2_INCLUDE_DIRS})

find_package(Curses REQUIRED)
include_directories(${CURSES_INCLUDE_DIRS})

find_package(OpenMP)
if (OpenMP_FOUND)
  include_directories(${OpenMP_C_INCLUDE_DIRS})
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
  add_compile_definitions(HAS_OPENMP)
endif()

include(TestBigEndian)
test_big_endian(BIG_ENDIAN_ARCH)
add_compile_definitions(BIG_ENDIAN_ARCH=${BIG_ENDIAN_ARCH})

#
# libgol
#

set(LIBGOL_FILES
  curses.c curses.h
  dummy.c dummy.h
  exception.c exception.h
  gol.c gol.h
  increments.c increments.h
  render.c render.h
  sdl.c sdl.h
  rle.c rle.h
  )
add_library(gol ${LIBGOL_FILES})
target_link_libraries(gol ${SDL2_LIBRARIES} ${CURSES_LIBRARIES} ${OpenMP_C_LIBRARIES})

#
# golrun
#

add_executable(golrun main.c)
target_link_libraries(golrun gol)



