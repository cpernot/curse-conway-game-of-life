#include <stdbool.h>

typedef enum {
	      NO_ERROR = 0,
	      INVALID_PARAMETER,
	      NO_MEMORY,
	      INVALID_FORMAT
} exception_code_t;

extern bool exception;

extern void exception_raise(exception_code_t code, char *message, ...);
extern void exception_get(exception_code_t *code, char **message);
extern void exception_clear();
