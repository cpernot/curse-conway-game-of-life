#ifndef RENDER_H
#define RENDER_H

#include <stdbool.h>

/* typedef pre-declarations */
typedef struct game *game_t;
typedef struct sdl_context *sdl_context_t;
typedef struct dummy_context *dummy_context_t;
typedef struct render_driver *render_driver_t;
typedef struct render_context *render_context_t;

struct render_context {
  render_driver_t driver;
  union {
    sdl_context_t sdl;
    dummy_context_t dummy;
  };
};


struct render_driver {
  char name[20];
  void (*init)(render_context_t rctx, game_t g, bool fullscreen);
  void (*close)(render_context_t ctx);
  void (*setup_next_frame)(render_context_t ctx);
  void (*render)(render_context_t ctx, game_t g);
  int (*get_width)(render_context_t ctx);
  int (*get_height)(render_context_t ctx);
  void (*activate_cell)(render_context_t ctx, int tid, int x, int y);
  void (*handle_events)(render_context_t ctx, game_t g);
};

extern render_context_t render_init(const char *driver_name, game_t g, bool fullscreen);
extern void render_close(render_context_t ctx);
extern void render_setup_next_frame(render_context_t ctx);
extern void render_render(render_context_t ctx, game_t g);
extern int render_get_width(render_context_t ctx);
extern int render_get_height(render_context_t ctx);
extern void render_activate_cell(render_context_t ctx, int tid, int x, int y);
extern void render_handle_events(render_context_t ctx, game_t g);


#endif
