#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include "exception.h"

bool exception = false;

static exception_code_t _code;
static char *_message = NULL;

void exception_raise(exception_code_t code, char *message, ...){
  va_list args;
  va_start(args, message);

  exception = true;
  _code = code;
  if (_message != NULL)
    free(_message);
  vasprintf(&_message, message, args);
  va_end(args);
}

void exception_get(exception_code_t *code, char **message) {
  *code = _code;
  *message = _message;
  exception = false;
}

void exception_clear() {
  exception = false;
  if (_message != NULL) {
    free(_message);
    _message = NULL;
  }
}
