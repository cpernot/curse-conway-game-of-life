#include "gol.h"

typedef struct curses_context *curses_context_t;

extern curses_context_t curses_init();
extern void curses_close(curses_context_t);
extern void curses_render(curses_context_t ctx, game_t g);
extern int curses_get_width(curses_context_t ctx);
extern int curses_get_height(curses_context_t ctx);
