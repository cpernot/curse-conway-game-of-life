#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <sys/time.h>
#include <time.h>
#include <getopt.h>
#include "gol.h"
#include "render.h"
#include "exception.h"
#include "render.h"
#include "rle.h"


char glider[] = {"#C This is a glider.\n"
		 "x = 3, y = 3\n"
		 "bo$2bo$3o!\n"
};

char gosper[] = {
"#N Gosper glider gun\n"
"#O Bill Gosper\n"
"#C A true period 30 glider gun.\n"
"#C The first known gun and the first known finite pattern with unbounded growth.\n"
"#C www.conwaylife.com/wiki/index.php?title=Gosper_glider_gun\n"
"x = 36, y = 9, rule = B3/S23\n"
"24bo11b$22bobo11b$12b2o6b2o12b2o$11bo3bo4b2o12b2o$2o8bo5bo3b2o14b$2o8b\n"
"o3bob2o4bobo11b$10bo5bo7bo11b$11bo3bo20b$12b2o!\n"
};

char twoguns[] = {
"#N twogun\n"
"#O V. Everett Boyer and Doug Petrie\n"
"#C The smallest known period-60 gun; it uses two copies of the Gosper\n"
"#C glider gun.\n"
"x = 39, y = 27, rule = b3/s23\n"
"27bo11b$25bobo11b$15b2o6b2o12b2o$14bo3bo4b2o12b2o$3b2o8bo5bo3b2o14b$3b\n"
"2o8bo3bob2o4bobo11b$13bo5bo7bo11b$14bo3bo20b$15b2o22b$26bo12b$27b2o10b\n"
"$26b2o11b4$$$$21b2o16b$9bobo10b2o15b$9bo2bo8bo17b$2o10b2o11b2o12b$2o8bo3b\n"
"2o8bobo12b$5b2o5b2o9bo6b2o7b$4bo4bo2bo10bo2bo2bo2bo6b$9bobo11bo6b3o6b$\n"
"24bobo5b3o4b$25b2o6bobo3b$35bo3b$35b2o!\n"
};

char destruction[] = {
"#N Gosper glider gun (glider destruction)\n"
"#O Dean Hickerson\n"
"#C Complete destruction of Gosper glider gun with two gliders\n"
"#C Glider destruction of the Gosper glider gun.\n"
"#C http://www.conwaylife.com/wiki/Gosper_glider_gun\n"
"#C http://www.conwaylife.com/patterns/gosperglidergungliderdestruction.rle\n"
"x = 47, y = 26, rule = B3/S23\n"
"bo$2bo$3o6$$$$$$15bo$15b4o$16b4o10b2o$5b2o9bo2bo9bobo$5b2o9b4o8b3o8b2o$15b\n"
"4o8b3o9b2o$15bo12b3o$29bobo$30b2o7$$$$$$$45b2o$44b2o$46bo!\n"
};

void sleep_sec(float sec_f){
  struct timespec ts;
  ts.tv_sec = (int) sec_f;
  ts.tv_nsec = (sec_f - ts.tv_sec) * 1E9;
  nanosleep(&ts, &ts);
}

static double elapsed(struct timeval *begin, struct timeval *end) {
  return fabs((end->tv_sec * 1e6 + end->tv_usec) -
	      (begin->tv_sec * 1e6 + begin->tv_usec));
}

int main(int argc, char **argv){
  time_t t;
  struct timeval begin, last_render, last_generation;

  srandom((unsigned) time(&t));
  double mutation_rate = -1;
  char *renderer_driver = NULL;
  bool fullscreen = false;
  double fps = -1;
  double gps = -1;
  int width = 800;
  int height = 800;
  int count = 0;
  bool paused = false;

  while (1){
    int option_index = 0;
    static struct option longoption[] = {
					 {"fps", required_argument, 0, 'f'},
					 {"gps", required_argument, 0, 'g'},
					 {"fullscreen", no_argument, 0, 'F'},
					 {"mutation", required_argument, 0, 'm'},
					 {"count", required_argument, 0, 'c'},
					 {"width", required_argument, 0, 'w'},
					 {"height", required_argument, 0, 'h'},
					 {"renderer", required_argument, 0, 'r'},
					 {"paused", required_argument, 0, 'p'},
					 {0, 0, 0, 0}
    };
    char c = getopt_long(argc,argv,"f:g:Fm:w:h:c:r:p", longoption, &option_index);
    if(c == -1){
      break;
    }
    switch(c){
    case 'm':
      mutation_rate = atof(optarg);
      break;
    case 'F':
      fullscreen = true;
      break;
    case 'f':
      fps = atof(optarg);
      break;
    case 'g':
      gps = atof(optarg);
      break;
    case 'c':
      count = atoi(optarg);
      break;
    case 'w':
      width = atoi(optarg);
      break;
    case 'h':
      height = atoi(optarg);
      break;
    case 'r':
      renderer_driver = strdup(optarg);
      break;
    case 'p':
      paused = true;
      break;
    }
  }

  if (renderer_driver == NULL)
    renderer_driver = strdup("sdl");

  game_t globalgame = game_init(width, height, mutation_rate, NULL);
  render_context_t renderer = render_init(renderer_driver, globalgame, fullscreen);

  if (paused) {
    game_pause_toggle(globalgame);
  }

  free(renderer_driver);

  if (renderer == NULL) {
    if (exception) {
      exception_code_t code;
      char *message;
      exception_get(&code, &message);
      printf("Exception #%d raised : %s\n", code, message);
      exit(code);
    } else {
      printf("An unkown error occured in render_init().\n");
      exit(1);
    }
  }

  last_render.tv_sec = 0;
  last_render.tv_usec = 0;

  /*
  game_empty(globalgame);
  if ( rle_put(globalgame, 118, 118, gosper) == -1) {
      exception_code_t code;
      char *message;
      exception_get(&code, &message);
      printf("Exception #%d raised : %s\n", code, message);
      exit(code);
  };
  char *newgun = rle_compress(globalgame, 118, 118, 36 , 9);
  rle_put(globalgame, 100, 250, newgun);
  */

  while (1){

    gettimeofday(&begin, NULL);

    if ((fps <= 0
	 || elapsed(&last_render, &begin)*1e-6 > 1/fps)) {
      render_render(renderer, globalgame);
      render_setup_next_frame(renderer);
      printf("\033[2K%d @ %f fps - Avg: %f fps\r",
	     game_age(globalgame),
	     1000/game_performance(globalgame),
	     1000/game_performance_average(globalgame));
      fflush(stdout);

      gettimeofday(&last_render, NULL);
    }

    render_handle_events(renderer, globalgame);

    if ((gps <= 0
	 || elapsed(&last_generation, &begin)*1e-6 > 1/gps)
	&& !game_paused(globalgame)) {
      if (mutation_rate > 0) game_mutation(globalgame);

      game_step(globalgame, renderer);

      gettimeofday(&last_generation, NULL);
      game_performance_record(globalgame, elapsed(&begin, &last_generation)*1e-3);
    }


    double wait_fps = 1/fps - elapsed(&last_render, &begin)*1e-6;
    double wait_gps = 1/gps - elapsed(&last_generation, &begin)*1e-6;
    double wait = wait_fps < wait_gps ? wait_fps : wait_gps;
    if (fps > 0 && gps > 0 && wait > 0) {
      sleep_sec(wait);
    }

    if (count != 0 && game_age(globalgame) > count) {
      printf("\n");
      break;
    }
  }
  render_close(renderer);
}
